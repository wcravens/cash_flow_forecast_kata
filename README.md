# Cash Flow Forecast

I want a command line utility that will allow me to present a
cashflow forcast into the future.  It must take these options:
StartDate (assumes now), Days To Forecast (the number of days to
forcast for), and Starting Balance.  A command line utility is
required but a WebApp would be really great.

The command line utility and the web app should take it's options
from a text configuration file.  An extension would be to add
command line options or browser form elements to configure these
options.

The output should look similar to any bank ledger.  There should be
columns for date, description, withdrawl, deposit and running
balance.  The columns should be in that order and left aligned.

The date should be fixed width to represent a date format such as 'Jan
01 2015' (thre char month, two char day of mont and four char year.

The withdrawl and deposit columns should be adequate width to handle
values up to 5 characters of integer part and 2 characters of
fractional part.

The final part of the output should include a small report to
present: Number of Days in the Report, Starting Balance, Ending
Balance, Total Income and Total Expense.

## Recurring Income

On Friday Every Two Weeks "Bob's Hardware" pays me $500

## Recurring Expenses

On the 15th of every month I pay $300 in 'Apartment Rental'

On the 27th of every month I pay $75 to 'Ripoff Internetworks'

Every Monday I take out $50 in cask for my 'Spending Money'

## Hints

    > 1234.567.toFixed(2)
    '1234.57'

sprintf 10.2 will format a float number right aligned.

    console.log(sprintf("Space Padded => %10.2f", 123.4567));
    console.log(sprintf("    _ Padded => %'_10.2f", 123.4567));
    console.log(sprintf("    0 Padded => %010.2f", 123.4567));
    console.log(sprintf(" Left align  => %.2f", 123.4567));

